# -*- coding: utf-8 -*-
"""
Second project from Code Academy course.
Originally developed in Jupyter.


Linear Regression is when you have a group of points on a graph, and you find 
a line that approximately resembles that group of points. A good Linear 
Regression algorithm minimizes the error, or the distance from each point to 
the line. A line with the least error is the line that fits the data the best. 
We call this a line of best fit.

We will use loops, lists, and arithmetic to create a function that will find 
a line of best fit when given a set of data.
"""

#returns y for y = mx + b 
def get_y(m, b, x):
  y = m*x + b
  return y

#returns error of fittng (difference between calculated and original y)
def calculate_error(m, b, point):
    x_point = point[0]
    y_point = point[1]
    return abs(get_y(m, b, x_point) - y_point)

#returns sum of errors for data set
def calculate_all_error(m, b, points):
    total_error = 0
    for point in points:
        total_error += calculate_error(m, b, point)
    return total_error

#returns best m and b of given range
def best_fit(datapoints, m_range, b_range):
    possible_ms = [m*0.1 for m in range(int(m_range[0]*10), int(m_range[1])*10)]
    possible_bs = [b*0.1 for b in range(int(b_range[0]*10), int(b_range[1])*10)]

    smallest_error = float("inf")
    best_m = 0
    best_b = 0
    
    for m in possible_ms:
        for b in possible_bs:
            if calculate_all_error(m, b, datapoints) < smallest_error:
                best_m = m
                best_b = b
                smallest_error = calculate_all_error(m, b, datapoints)
    
    return best_m, best_b, smallest_error
    
### Ener your data here:
datapoints = [(1, 2), (2, 0), (3, 4), (4, 4), (5, 3)]
m_range = [-10.0, 10.1]
b_range = [-20.0, 20.1]
x = 6
###

best_m, best_b, smallest_error = best_fit(datapoints, m_range, b_range)
y = get_y(best_m, best_b, x)
print("The best fit is m= {m} and b= {b} (error= {error})".format(m=round(best_m, 1), b=round(best_b, 1), error=round(smallest_error, 1)))
print("For x= {x}: y= {y}".format(x=x, y=round(y, 1)))


'''
TO DO: 
- error lub dokladnosc (miejsca po przecinku) <<< dlugosc listy range: find()
- GUI?
'''