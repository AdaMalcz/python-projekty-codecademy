# -*- coding: utf-8 -*-
"""
Unit Test fot CA2_linear_regretion code.
"""

import unittest
from CA2_linear_regretion import get_y, calculate_error, calculate_all_error, best_fit

datapoints = [(1, 2), (2, 0), (3, 4), (4, 4), (5, 3)] #testing data set
m_range = [-10.0,10.1] #testing m range
b_range = [-20.0,20.1] #testing b range
best_m, best_b, smallest_error = best_fit(datapoints, m_range, b_range) #calculated values

class CA2_Test(unittest.TestCase):

#get_y tests    
    def test_y_1_0_7_equals_7(self):
        self.assertTrue(get_y(1, 0, 7) == 7)
    def test_y_5_10_13_equals_25(self):
        self.assertTrue(get_y(5, 10, 3) == 25)

#calculate_error tests
    def test_error_1_0_3_3_equals_1(self):
        self.assertTrue(calculate_error(1, 0, (3, 3)) == 0)
    def test_error_1_0_3_4_equals_1(self):
        self.assertTrue(calculate_error(1, 0, (3, 4)) == 1)
    def test_error_1_m1_0_3_4_equals_1(self):
        self.assertTrue(calculate_error(1, -1, (3, 3)) == 1)
    def test_error_m1_1_0_3_4_equals_1(self):
        self.assertTrue(calculate_error(-1, 1, (3, 3)) == 5)

#calculate_all_error tests
    def test_all_error_1_0_data_equals_0(self):
        self.assertTrue(calculate_all_error(1, 0, [(1, 1), (3, 3), (5, 5), (-1, -1)]) == 0)
    def test_all_error_1_1_data_equals_0(self):
        self.assertTrue(calculate_all_error(1, 1, [(1, 1), (3, 3), (5, 5), (-1, -1)]) == 4)
    def test_all_error_1_m1_data_equals_0(self):
        self.assertTrue(calculate_all_error(1, -1, [(1, 1), (3, 3), (5, 5), (-1, -1)]) == 4)
    def test_all_error_m1_1_data_equals_0(self):
        self.assertTrue(calculate_all_error(-1, 1, [(1, 1), (3, 3), (5, 5), (-1, -1)]) == 18)

#best_fit tests
    def test_best_fit_m_0p3(self):
        self.assertAlmostEqual(best_m, 0.3)
    def test_best_fit_b_1p7(self):
        self.assertAlmostEqual(best_b, 1.7)
    def test_best_fit_error_4p9(self):
        self.assertAlmostEqual(smallest_error, 5.0)

        
if __name__ == '__main__':
    unittest.main()

'''  
#every point in this dataset lies upon y=x, so the total error should be zero:
datapoints = [(1, 1), (3, 3), (5, 5), (-1, -1)]
print(calculate_all_error(1, 0, datapoints))

#every point in this dataset is 1 unit away from y = x + 1, so the total error should be 4:
datapoints = [(1, 1), (3, 3), (5, 5), (-1, -1)]
print(calculate_all_error(1, 1, datapoints))

#every point in this dataset is 1 unit away from y = x - 1, so the total error should be 4:
datapoints = [(1, 1), (3, 3), (5, 5), (-1, -1)]
print(calculate_all_error(1, -1, datapoints))


#the points in this dataset are 1, 5, 9, and 3 units away from y = -x + 1, respectively, so total error should be
# 1 + 5 + 9 + 3 = 18
datapoints = [(1, 1), (3, 3), (5, 5), (-1, -1)]
print(calculate_all_error(-1, 1, datapoints))

#A: sprawdzanie przypadków brzegowych (liczby od 1)
    def test_n_0(self):
        #czy program wykona się dla n=0? spodzewany wynik: ValueError
        with self.assertRaises(ValueError):
            F(0)
        
    def test_n_1(self):
        #czy program wykona się dla n=1? spodziewany wynik: 1
        self.assertTrue(F(1)==1)
        
    def test_n_2(self):
        #czy program wykona się dla n=2? spodziewant=y wynik: 5 (1^2 + 2^2 =1 +4 = 5)
        self.assertTrue(F(2)==5)

#A: sprawdzanie przypadków nie będących brzegowymi
    def test_n_3(self):
        #spodziwany wynik: 14 (1+4+9)
        self.assertTrue(F(3)==14)
        
    def test_n_5(self):
        #spodziewany wynik: 30 (1+4+9+16+25)
        self.assertTrue(F(5)==55)
        
    def test_n_10(self):
        #spodziewany wynik: 385 (1+4+9+16+25+36+49+64+81+100)
        self.assertTrue(F(10)==385)

#B: sprawdzanie wyjątków
    def test_n_niecalkowite(self):
        #czy dla liczby niecalkowitej wyrzuci TypeError?
        with self.assertRaises(TypeError):
            F(3.2)
            
    def test_n_ujemne(self):
        #czy dla liczby ujemnej wyrzuci ValueError?
        with self.assertRaises(ValueError):
            F(-1)
        with self.assertRaises(ValueError):
            F(-50)

print("\n")
print("Sprawdzono warunki brzegowe dla minimalnej warosci n[1] (0,1,2), kilku innych przypadkow oraz obsluge wyjatkow. Jezeli powyzej wysiwetlilo {OK} to wszystkie testy na pewno zakonczyly sie sukcesem.")

if __name__ == '__main__':
    unittest.main()


A. Napisz  testy  jednostkowe  sprawdzające  poprawność  działania  funkcji 
suma_kwadratow. Uwzględnij przypadki brzegowe oraz  kilka przypadków nie 
będących przypadkami brzegowymi.


B. Napisz testy jednostkowe sprawdzające, czy funkcja generuje określone wyjątki:

a. wyjątek typu TypeError w przypadku, gdy argument wejściowy funkcji nie jest 
liczbą całkowitą,

b. wyjątek typu ValueError w przypadku, gdy argument wejściowy funkcji jest 
liczbą ujemną.

Uruchom testy i upewnij się, że wszystkie kończą się sukcesem.
'''